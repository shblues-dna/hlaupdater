import java.io.File;
import java.time.LocalDateTime;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import jplib.JpApp;
import jplib.JpFile;
import jplib.JpLog;
import jplib.JpMsg;
import jplib.JpStr;
import jplib.JpTheme;
import jplib.JpUpdater2;

/*
	- Читается текущая версия приложения  
        1) из HLAExpert.jar->app_ver_info.xml
	    2) если не найдено, то из файла ver_info.xml
	    3) если не найдено, то присваивается номер "1.1.1"

    - Проверяется версия приложения из файла /update/hla_expert/ver_info.xml  (/<loc_dir>/<loc_unit_path>/ver_info.xml)

    - Сравниваются номера

    - Если совпадает, то выход и запуск HLAExpert
      Если НЕ совпадает, то обновление
      
	- Ожидание остановки HLAExpert (Проверка oApp.lock_App_File("HLAExpert"))  

    - Файл архива, прочитанный из /update/hla_expert/ver_info.xml   (app_upd.loc_arc_file)
      распаковывается во временный каталог в папке  /temp

    - Далее все из каталога "hla_expert" (это каталог внутри архива app_upd.arc_ver_path) копируется в (app_upd.dist_path)
    
    - Запуск HLAExpert
*/
public class HLAUpdater extends Application  {

	// **************************************************
	// prm
	// **************************************************
	public int 		k_run 			= 1;
	public String 	app_name		= "HLA-Эксперт";
	public String 	updater_name	= "HLAUpdater";
	
	// **********************************************************************
    /**
     * Запуск приложения
     */
	// **********************************************************************
    @Override
    public void start(Stage stage) {
    	
    	// ==================================================
		// 
		// ==================================================
    	JpApp oApp 			= JpApp.getInstance();
    	int k_go			= 0;	
    	boolean k_update	= false;	

        // ==================================================
        // log
        // ==================================================
        JpApp.k_Log_Out	= true;
        JpApp.log_pref	= "hlaexpert_log";
        JpLog app_log = JpApp.get_Log();
        app_log.k_clear_cr = 1;
    	
		// ==================================================
		// Updater 
		// ==================================================
		JpUpdater2 app_upd = new JpUpdater2();
		app_upd.ftp_unit_path		= "hla_expert";
		app_upd.ftp_ver_info_file	= "ver_info.xml";
		app_upd.loc_unit_path		= "hla_expert";
		app_upd.arc_ver_path		= "hla_expert";				// Имя каталога в архиве из котрого копировать все в dist_path
		app_upd.dist_path			= JpApp.get_App_Path();	// Основной каталог, назначения в который копируются файлы +новой версии 
		app_upd.tmp_path			= ""; 
		app_upd.task_caption		= "Установка программных модулей "+app_name;

    	// ==================================================
		// Создать форму на экране
    	// ==================================================
    	JpApp.app_pane 	= new BorderPane();
		JpApp.app_stage	= new Stage(); 
		JpApp.app_scene	= new Scene(JpApp.app_pane, 800, 600);
		JpApp.app_stage.setScene(JpApp.app_scene);
        // Заголовок
		JpApp.app_stage.setTitle("Обновление "+app_name);

        // ==================================================
        // Темы
        // ==================================================
        JpTheme app_skin	= null;

		// ======================================================================
        // Setup mainStage, rootPane, mainScene 
		// ======================================================================
        JpApp.create_App_Form();
        JpApp.set_App_Caption("Обновление "+app_name);
        JpApp.set_App_Icone("images/hla_icone.png");
        
        // Default
        app_skin = new JpTheme();
        app_skin.set_Bt_Tool_Size(32);
        app_skin.set_Bt_Win_Flat_Size(24);
        app_skin.set_Theme_Path("/themes/default");
        app_skin.set_Css_File("hlaexpert_default.css");
        JpApp.add_Theme("default", app_skin);

        // Если схема не задана, то пытаемся читать из параметров
        JpApp.set_Theme("default");
        JpApp.show_App_Form();
	    	
		// ==================================================
		// Прочитать версию приложения   
		// ==================================================
		// Прочитать версию приложения из Jar  
        app_upd.app_ver_num = app_upd.get_Ver_Info_From_Jar("HLAExpert.jar", "app_ver_info.xml");
		// Прочитать версию приложения из ver_info.xml 
        if (app_upd.app_ver_num.equals("")) {
            String ver_info_str		= "";
            String ver_info_file 	= JpApp.get_App_Path()+File.separator+app_upd.ftp_ver_info_file;
            ver_info_str = JpFile.FileToStr(ver_info_file);
            app_upd.app_ver_num = JpStr.strExtr(ver_info_str, "<ver_num>", "</ver_num>");
        }
        if (app_upd.app_ver_num.equals("")) {
            app_upd.app_ver_num = "1.1.1";
        }
	        
    	// ==================================================
		// Проверить наличие новой версии приложения
		// Обновляем, если необходимо 
		// ==================================================
        k_update = app_upd.version_Num_Compare_UpdVerInfo();
		if (k_update) {

	    	// ==================================================
			// Блокировка запуска приложения
			// ==================================================
			k_go = 1;
	    	LocalDateTime time_start = LocalDateTime.now();
	    	LocalDateTime time_now = LocalDateTime.now();
	        while (!oApp.lock_App_File("HLAExpert")) {
	        	time_now = LocalDateTime.now();
	        	// Время прошло - заблокировать не удалось
	        	if (time_now.minusSeconds(20).isAfter(time_start)) {
	        		k_go = 0;
	        		break;
	        	}
	        }
			
			// ==================================================
			// Заблокировать не удалось
			// Уже выполнятеся
			// ==================================================
			if (k_go==0) {
				JpMsg.mErr(app_name+" сейчас выполняется.\nДля обновления закройте приложение и выполните "+updater_name+"\n");
				JpApp.write_Log("["+updater_name+"] "+app_name+" сейчас выполняется. Для обновления закройте приложение и выполните "+updater_name);
		    	 // Завершить ФСЕ!
		    	 Platform.exit();
			}
			
			// ==================================================
			// Событие при успешном завершении "Загрузка новой версии программных модулей"
			// ==================================================
	        app_upd.verison_update_succeed = (WorkerStateEvent event)-> {

	    		String ver_name	= ""; 
	    		ver_name 	= JpStr.strExtr(app_upd.ftp_ver_info_str,"<ver_name>","</ver_name>");
	        	
	        	// ==================================================
				// Есть ошибки
				// ==================================================
				if (app_upd.err_cnt>0) {
					app_upd.show_Err();
				} else {
					JpMsg.mInfoText("Установлены программные модули\n"+ver_name);
					JpApp.write_Log("["+updater_name+"] Установлены программные модули "+ver_name);
				}
				
				// Удалим лишний ver_info.xml
				JpFile.del_File(JpApp.get_App_Path()+File.separator+app_upd.ftp_ver_info_file);
				
				// ==================================================
				// 
				// ==================================================
				app_Run();
				return;
	        };

	        app_upd.verison_update_failed = (WorkerStateEvent event)-> {
	        	// ==================================================
				// Есть ошибки
				// ==================================================
				if (app_upd.err_cnt>0) {
					app_upd.show_Err();
				} else {
					JpMsg.mErr("При установке программных модулей возникала ошибка!");
					JpApp.write_Log("["+updater_name+"] При установка программных модулей возникала ошибка!");
				}
				app_Run();
				return;
	        };
	        
	        // ==================================================
			// 
			// ==================================================
	        app_upd.version_Update_Files(1);
	        
		} else {

			String ver_name	= ""; 
    		ver_name 	= JpStr.strExtr(app_upd.ftp_ver_info_str,"<ver_name>","</ver_name>");
    		if (ver_name.equals("")) {
    			JpMsg.mInfoText("Обновление "+app_name+" не требуется");
				JpApp.write_Log("["+updater_name+"] Обновление "+app_name+" не требуется");
    		} else {
				JpMsg.mInfoText("Обновление "+app_name+" не требуется\nУстановлены программные модули\n"+ver_name);
				JpApp.write_Log("["+updater_name+"] Обновление "+app_name+" не требуется. Установлены программные модули "+ver_name);
    		}
			app_Run();
		}
    	
    }

	// **********************************************************************
    /**
     * Прочитать версию из jar файла
     */
	// **********************************************************************

    
	// **********************************************************************
	// 
	// **********************************************************************
    public void app_Run() {
    	
    	if (this.k_run==0) {
    		return;
    	}

    	// ==================================================
		// Разблокировать
		// ==================================================
    	JpApp oApp 	= JpApp.getInstance();
    	oApp.unLockInstance();

    	// ==================================================
		// Запустить процесс
		// ==================================================
    	if (JpFile.check_File(JpApp.get_App_Path()+File.separator+"HLAExpert.jar")) {
	    	try {
	    		String mem = "-Xmx" + Math.round((double)(Runtime.getRuntime().maxMemory()+Runtime.getRuntime().freeMemory())/(1024.0*1024.0)+1)+"m";
	    		//JpMsg.mInfoText("Доступная память "+mem);
	        	ProcessBuilder pb = new ProcessBuilder(JpApp.get_App_Path()+File.separator+JpFile.set_File_Str((JpApp.isWindows() ? "jre/bin/javaw" : "jre/bin/java")), mem, "-jar", "HLAExpert.jar");
	    		pb.start();
	    	} catch (Exception ex) {
	    		JpApp.system_Out(ex.getMessage());
	    	}
    	}

    	 // Завершить ФСЕ!
    	 Platform.exit();
    }
    
    
	// **********************************************************************
	// 
	// **********************************************************************
    public static void main(String[] args) {
    	Application.launch(args);
    	System.exit(0);
    }

    
}
